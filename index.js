import { AppRegistry, YellowBox, Platform } from 'react-native';
import App from './src/index';

YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader', "Warning: Can't call setState (or forceUpdate) on an unmounted component."]);

AppRegistry.registerComponent('ChasitApp', () => App);

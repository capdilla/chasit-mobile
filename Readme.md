Install 

```sh
git clone https://gitlab.com/capdilla/chasit-mobile.git
cd chasit-mobile
npm i
```

Run
```sh
react-native run-ios 

or

react-native run-android
```


How to install react native

https://facebook.github.io/react-native/docs/getting-started.html
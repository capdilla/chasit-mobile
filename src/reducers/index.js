import { combineReducers } from 'redux';
import user from "./user";
import reports from "./reports";
import chat from "./chat";

export default combineReducers({
  user,
  reports,
  chat
})
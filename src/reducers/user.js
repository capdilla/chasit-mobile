import { createAction, handleActions } from 'redux-actions';
import initialState from './initialState.js';
import apis from '../actions/apis';
import _ from 'lodash';
import update from 'react-addons-update'; // ES6

import { AsyncStorage } from 'react-native';

export const fetchUser = createAction('FETCH_USER', apis.login);
export const fetching = createAction('FETCHING');
export const setUser = createAction('SET_USER');
export const setNotificationPushId = createAction('SET_NOTIFICATION_PUSH_ID');
export const logOutUser = createAction('LOG_OUT_USER');
export const register = createAction('REGISTER', apis.register);

export const getVechicles = createAction('GET_VEHICLES', apis.vehicles);
export const updateVechicles = createAction('UPDATE_VEHICLES', apis.updateVehicles);
export const createVechicles = createAction('CREATE_VEHICLES', apis.createVehicles);
export const deleteVechicles = createAction('DELETE_VEHICLES', apis.deleteVehicles);


const initialUser = { ...initialState.user }

const saveUser = async (data) => {
  const user = await AsyncStorage.setItem("UserData", JSON.stringify(data));
}


export const user = (state, action) => {
  if (action.payload.success) {
    console.log(action)
    const user = {
      ...action.payload.user,
      token: action.payload.token,
      guest: false,
      fetching: false
      // notification_push_user_id: action.payload.notification_push_user_id
    }
    saveUser(user);
    return ({
      ...state,
      ...user,
      vehicles: [...action.payload.vehicles]
    })
  } else {
    return ({
      ...state,
      message: action.payload.message
    })
  }
};

export default handleActions({
  [register]: (state, action) => user(state, action),
  [fetchUser]: (state, action) => user(state, action),
  [getVechicles]: (state, action) => {
    if (action.payload.success) {
      return ({
        ...state,
        vehicles: [...action.payload.vehicles]
      })
    } else {
      return ({
        ...state
      })
    }
  },
  [updateVechicles]: (state, action) => {
    if (action.payload.success) {
      const indx = _.findIndex(state.vehicles, { id: action.payload.vehicle.id })
      return ({
        ...state,
        vehicles: update(state.vehicles, {
          [indx]: {
            $merge: { ...action.payload.vehicle }
          }
        })
      })
    } else {
      return ({
        ...state
      })
    }

  },
  [createVechicles]: (state, action) => {
    if (action.payload.success) {
      return ({
        ...state,
        vehicles: update(state.vehicles, {
          $push: [action.payload.vehicle]
        })
      })
    } else {
      return ({
        ...state
      })
    }

  },
  [deleteVechicles]: (state, action) => {
    if (action.payload.success) {
      const vehicles = [...state.vehicles];
      _.remove(vehicles, (v) => v.id == action.payload.vehicle.id);
      return ({
        ...state,
        vehicles
      })
    } else {
      return ({
        ...state
      })
    }

  },
  [fetching]: (state, action) => ({
    ...state,
    fetching: true
  }),
  [logOutUser]: (state, action) => {
    try {
      const { notification_push_user_id } = state
      return ({
        ...state,
        ...initialUser,
        notification_push_user_id
      })
    } catch (error) {
      console.log(error)
    }
  },
  [setUser]: (state, action) => ({
    ...state,
    ...action.payload
  }),
  [setNotificationPushId]: (state, action) => ({
    ...state,
    ...action.payload
  })
}, { ...initialState.user });
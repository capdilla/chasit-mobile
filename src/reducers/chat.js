import { createAction, handleActions } from 'redux-actions';
import initialState from './initialState.js';
import apis from '../actions/apis';
import _ from 'lodash';
import update from 'react-addons-update'; // ES6

import { AsyncStorage } from 'react-native';


export const getChats = createAction('GET_CHATS', apis.getMyChats);
export const updateVechicles = createAction('UPDATE_VEHICLES', apis.updateVehicles);
export const createVechicles = createAction('CREATE_VEHICLES', apis.createVehicles);
export const deleteVechicles = createAction('DELETE_VEHICLES', apis.deleteVehicles);


export default handleActions({
  [getChats]: (state, action) => {
    if (action.payload.success) {
      return ({
        ...state,
        chats: [...action.payload.chats]
      })
    } else {
      return ({
        ...state
      })
    }
  },
}, { ...initialState.chat });
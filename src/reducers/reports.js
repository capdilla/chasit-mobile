import { createAction, handleActions } from 'redux-actions';
import initialState from './initialState.js';
import apis from '../actions/apis';
import _ from 'lodash';
import update from 'react-addons-update'; // ES6


export const defaultReport = createAction('DEFAULT_REPORT');
export const getReports = createAction('GET_REPORTS', apis.getReports);
export const createReport = createAction('CREATE_REPORT', apis.createReport);
export const deleteReport = createAction('DELETE_REPORT', apis.deleteReport);

const defaultState = { ...initialState.create }

export default handleActions({
  [defaultReport]: (state, action) => ({
    ...state,
    create: { ...defaultState }
  }),
  [getReports]: (state, action) => {
    if (action.payload.success) {
      return ({
        ...state,
        user_report: [...action.payload.reports]
      })
    } else {
      return ({
        ...state
      })
    }
  },
  [createReport]: (state, action) => {
    if (action.payload.success) {
      return ({
        ...state,
        create: { done: true, message: action.payload.message, success: action.payload.success }
      })
    } else {
      return ({
        ...state,
        create: { done: true, message: action.payload.message, success: action.payload.success }
      })
    }
  },
  [deleteReport]: (state, action) => {
    if (action.payload.success) {
      const user_report = [...state.user_report];
      _.remove(user_report, (v) => v.id == action.payload.report.id);
      return ({
        ...state,
        user_report
      })
    } else {
      return ({
        ...state
      })
    }
  }
}, { ...initialState.reports });
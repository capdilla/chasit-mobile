import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';


const ButtonInfo = ({ textUp, textDown, textColor, view, style, item, onPress }) => (

  (!view) ?
    <TouchableOpacity onPress={() => onPress()} style={[styles.main, style]}>
      <Text style={{ color: textColor, fontSize: 15, fontWeight: '700' }}>{textUp}</Text>
      <Text style={{ color: textColor, fontSize: 14 }}>{textDown}</Text>
    </TouchableOpacity>
    :
    <View style={[styles.main, style]}>
      <Text style={{ color: textColor, fontSize: 15, fontWeight: '800' }}>{textUp}</Text>
      <Text style={{ color: textColor, fontSize: 14 }}>{textDown}</Text>
    </View >
);

const styles = StyleSheet.create({
  main: {
    height: 65,
    width: 110,
    borderRadius: 8,
    backgroundColor: '#38c874',
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingHorizontal: 10,
    marginHorizontal: 5,
    marginTop: 10,
  }
})


ButtonInfo.defaultProps = {
  onPress: () => { },
  textUp: 'Car Name',
  textDown: 'Coche',
  view: false,
  style: {},
  textColor: '#fff'
}

export default ButtonInfo;
import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, KeyboardAvoidingView, Platform } from 'react-native';
import { withState, withHandlers, compose, branch, renderComponent } from 'recompose'
import { Kohana } from 'react-native-textinput-effects';
import Icon from 'react-native-vector-icons/Feather';
import { Actions } from 'react-native-router-flux';
import Spinner from 'react-native-spinkit';


//redux 
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchUser } from '../reducers/user';


const SpinnerComp = (props) => (
  <View style={{ ...alignCenter, flex: 1 }}>
    <Spinner
      color={"#27518b"}
      isVisible
      size={80}
      type="9CubeGrid" />
  </View>
)

const enhance = compose(
  branch(
    props => props.user.fetching || (Platform.OS == 'android' && props.user.notification_push_user_id === ''),
    renderComponent(SpinnerComp)
  ),
  withState('mail', 'setMail', ''),
  withState('password', 'setPassword', ''),
  withHandlers({
    onChangeMail: ({ setMail }) => (val) => setMail(val),
    onChangePass: ({ setPassword }) => (val) => setPassword(val),
  })
);

const LoginView = enhance(({ mail, password, onChangeMail, onChangePass, actions, user }) => (
  <View style={{ ...alignCenter, flex: 1 }}>
    <KeyboardAvoidingView
      behavior="padding"
      enabled
      style={{ marginHorizontal: 15, flex: 1, justifyContent: 'center', width: 280, }}
    >
      {console.log(user, "en login View")}
      <Text style={styles.text}> Ingresa tu mail o registrate</Text>
      <View style={{ height: 60, marginVertical: 10, marginHorizontal: 15 }}>
        <Kohana
          style={{ backgroundColor: '#E0EDF6' }}
          value={mail}
          onChangeText={onChangeMail}
          label={'Email'}
          iconClass={Icon}
          iconName={'mail'}
          iconColor={'#2C7FB2'}
          labelStyle={{ color: '#5494BF' }}
          inputStyle={{ color: '#5494BF' }}
        />
      </View>
      <View style={{ height: 60, marginVertical: 10, marginHorizontal: 15 }}>
        <Kohana
          style={{ backgroundColor: '#E0EDF6' }}
          label={'Password'}
          iconClass={Icon}
          iconName={'lock'}
          iconColor={'#2C7FB2'}
          labelStyle={{ color: '#5494BF' }}
          inputStyle={{ color: '#5494BF' }}
          secureTextEntry
          value={password}
          onChangeText={onChangePass}
        />
      </View>
      <Text style={{ color: 'red', textAlign: 'center' }}>{user.message}</Text>
      <TouchableOpacity
        onPress={() => actions.fetchUser({ mail, password, notification_push_user_id: user.notification_push_user_id })}
        style={styles.button}>
        <Text style={[styles.text, { fontSize: 15, color: '#C6F1FF' }]}>Logear</Text>
      </TouchableOpacity>
      <View style={{ backgroundColor: 'transparent', borderBottomWidth: 1, borderColor: '#94C3E3', }} />

      <TouchableOpacity
        onPress={() => Actions.modal()}
        style={styles.button}>
        <Text style={[styles.text, { fontSize: 15, color: '#C6F1FF' }]}>Registrarse</Text>
      </TouchableOpacity>

    </KeyboardAvoidingView>
  </View>
));


const alignCenter = {
  justifyContent: 'center',
  alignItems: 'center'
}

const shadow = {
  shadowColor: "#000000",
  shadowOpacity: 0.12,
  shadowRadius: 5,
  elevation: 2,
  shadowOffset: {
    height: 1,
    width: 0
  },
}
const styles = StyleSheet.create({
  big: {
    ...alignCenter,
    ...shadow,
    height: 200,
    width: 200,
    backgroundColor: '#f9f7f8',
    borderRadius: 100
  },
  medium: {
    ...alignCenter,
    ...shadow,
    height: 160,
    width: 160,
    backgroundColor: '#f8f8fa',
    borderRadius: 100
  },
  small: {
    ...alignCenter,
    ...shadow,
    height: 120,
    width: 120,
    backgroundColor: '#ffffff',
    borderRadius: 100
  },
  text: {
    fontSize: 18,
    fontFamily: 'Arial',
    fontWeight: 'bold',
    color: '#4991C3',
    textAlign: 'center'
  },
  button: {
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
    marginHorizontal: 15,
    ...shadow,
    backgroundColor: '#67ADDB'
  }
})

export default connect(
  state => ({
    user: state.user,
    fetching: state.fetching
  }),
  dispatch => ({
    actions: bindActionCreators({ fetchUser }, dispatch)
  })
)(LoginView);
import React, { Component } from 'react';
import OneSignal from 'react-native-onesignal'; // Import package from node modules
import { View, Platform, Alert } from 'react-native';
import { Actions } from 'react-native-router-flux'

//redux 
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setNotificationPushId } from '../reducers/user';

export const forceNPUI = (id) => {
  OneSignal.init("72fd08d6-56e9-49b0-8f01-a6b4395c1d62");
  OneSignal.getPermissionSubscriptionState((subscriptionState) => {
    id(subscriptionState.userId)
    // this.props.actions.setNotificationPushId({
    //   notification_push_user_id: subscriptionState.userId
    // })
  })
}

class NotificationPush extends Component {

  componentWillMount() {

    this._onOpened = this.onOpened.bind(this)
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this._onOpened);

    OneSignal.init("72fd08d6-56e9-49b0-8f01-a6b4395c1d62");
    OneSignal.setLogLevel(6, 0);
    OneSignal.inFocusDisplaying(2);
    OneSignal.configure()

    this._onIds = this.onIds.bind(this)

    OneSignal.addEventListener('ids', this._onIds);
    OneSignal.getPermissionSubscriptionState((subscriptionState) => {
      // this.props.actions.setNotificationPushId({
      //   notification_push_user_id: subscriptionState.userId
      // })
    })


  }

  onEmailRegistrationChange(registration) {
    console.log("onEmailRegistrationChange: ", registration);
  }

  onReceived(notification) {
    Alert.alert('Hola', 'hola')
    console.log("Notification received: ", notification);
  }

  onOpened(openResult) {
    const { additionalData } = openResult.notification.payload
    if (additionalData.type == 'report') {
      Actions.report_message({ report: additionalData.data })
    }
    if (additionalData.type == 'chat') {
      Actions.chat({ chat_main_id: additionalData.data.chat_main_id })
    }
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  }

  onIds(device) {
    // console.log("here")
    // console.log('Device info: ', device);
    // Alert.alert("Puta madre alfin " + device.userId)
    this.props.deviceId(device.userId)
  }

  render() { return <View /> }
}

export default connect(
  state => ({
    user: state.user,
  }),
  dispatch => ({
    actions: bindActionCreators({ setNotificationPushId }, dispatch)
  })
)(NotificationPush)
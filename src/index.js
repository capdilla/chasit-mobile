import React from 'react';

import { Provider } from 'react-redux';

import store from './store'
import StartApp from './startApp'

export default () => (
  <Provider store={store()}>
    <StartApp />
  </Provider>
)
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  StatusBar,
  AsyncStorage
} from 'react-native';

import {
  Scene,
  Router,
  Tabs,
  Stack,
  Modal
} from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Feather';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setUser, getVechicles, setNotificationPushId } from './reducers/user';
import { getReports } from './reducers/reports'

import OneSignal from 'react-native-onesignal'; // Import package from node modules
import NotificationPush from './components/NotificationPush'

import Spinner from 'react-native-spinkit';

import Report from "./screens/Report"
import ReportDetail from "./screens/ReportDetail"
import Chat from './screens/Chat';
import ChatList from './screens/ChatList';
import User from './screens/User';
import Register from './screens/Register/index';
import ReportMessage from './screens/ReportMessage';


const TabIcon = ({ name }) => (
  <View style={{ flex: 1, height: 100, width: 80, justifyContent: 'center', alignItems: 'center' }}>
    <Icon name={name} color={"#C1DBEE"} size={20} />
  </View>
);

const TabView = () => (
  <View>
    <Text>Tab view paps</Text>
  </View>
);


const App = () => (
  <SafeAreaView style={styles.container}>
    <Router>
      <Scene key="root">
        <Modal key="r_modal">
          <Tabs
            tabBarPosition="bottom"
            key="tabbar"
            swipeEnabled={false}
            showLabel={false}
            tabBarStyle={{ backgroundColor: '#b5d6ec' }}
            activeBackgroundColor="#27518b"
            inactiveBackgroundColor="#086799"
          >
            <Stack
              key="tab_1"
              title="chat"
              tabBarLabel="TAB #1"
              icon={() => <TabIcon name={"message-square"} />}
            >
              <Scene
                key="tab_1_1"
                component={() => <ChatList />}
                hideNavBar
              />
            </Stack>
            <Stack
              key="tab_2"
              title="Report"
              tabBarLabel="TAB #1"
              icon={() => <TabIcon name={"zap"} />}
              initial
            >
              <Scene
                key="tab_2_report"
                component={() => <Report />}
                hideNavBar
              />
            </Stack>
            <Stack
              key="tab_3"
              title="User"
              tabBarLabel="TAB #1"
              icon={() => <TabIcon name={"user"} />}
            >
              <Scene
                key="tab_1_1"
                component={() => <User />}
                hideNavBar
              />
            </Stack>
          </Tabs>
          <Scene
            key="report_detail"
            component={() => <ReportDetail />}
            hideNavBar
            hideTabBar
            headerMode={'float'}
            back={false}
          />
          <Scene
            key="chat"
            component={Chat}
            hideNavBar

            hideTabBar
            headerMode={'float'}
            back={false}
          />
          <Scene
            key="report_message"
            component={ReportMessage}
            hideNavBar

            hideTabBar
            headerMode={'float'}
            back={false}
          />
          <Scene
            key="modal"
            hideNavBar
            hideTabBar
            hideStatusBar
            component={Register} />
        </Modal>
      </Scene >
    </Router>
  </SafeAreaView >
);

class StartApp extends Component {
  constructor() {
    super();
    this.state = {
      userFetch: false
    }
  }

  async componentWillMount() {
    const user = await AsyncStorage.getItem("UserData")
    if (user) {
      await this.props.actions.setUser(JSON.parse(user));
      await this._getMoreData(JSON.parse(user))
      this.setState({ userFetch: true })
    } else {
      this.setState({ userFetch: true })
    }
  }

  _getMoreData(user) {
    this.props.actions.getVechicles({ token: user.token })
    this.props.actions.getReports({ auth: { token: user.token } })
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        {Platform.OS == 'android' ?
          <NotificationPush deviceId={(id) =>
            this.props.actions.setNotificationPushId({
              notification_push_user_id: id
            })
          } />
          :
          null
        }
        {(this.state.userFetch) ? <App />
          :
          <View style={{ justifyContent: 'center', alignContent: 'center', flex: 1 }}>
            <Spinner
              color={"#27518b"}
              isVisible
              size={80}
              type="9CubeGrid" />
          </View>}
      </View>
    )

  }
}

export default connect(
  state => ({
    user: state.user
  }),
  dispatch => ({
    actions: bindActionCreators({ setUser, getVechicles, setNotificationPushId, getReports }, dispatch)
  })
)(StartApp);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#b5d6ec',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: 'blue',
    marginBottom: 5,
  },
});

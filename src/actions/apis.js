import axios from 'axios';

export const url = (!__DEV__) ? 'http://192.168.1.38:3000/' : 'http://167.99.197.245:3000/';


const headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json',
};

const default_config = {
  baseURL: url,
  ...headers
}
const publicServer = axios.create({
  ...default_config
});

const privateServer = (token) => axios.create({
  baseURL: `${default_config.baseURL}private/`,
  headers: {
    ...default_config.headers,
    'Authorization': `Bearer ${token}`,
  }
});

export default {
  login: async (parms) => {
    try {
      const response = await publicServer.post('auth/login', { ...parms })
      return { ...response.data, notification_push_user_id: parms.notification_push_user_id }
    } catch (error) {
      console.log(error)
      return
    }
  },
  register: async (parms) => {
    try {
      const response = await publicServer.post('users/createUser', { ...parms })
      return { ...response.data }
    } catch (error) {
      console.log("hola")
    }
  },
  vehicles: async (params) => {
    try {
      const response = await privateServer(params.token).get('car/mycars');
      return { ...response.data }
    } catch (error) {
      console.log(error)
      return { success: false }
    }
  },
  updateVehicles: async (params) => {
    try {
      const response = await privateServer(params.auth.token).put(`car/edit/${params.body.id}`, { ...params.body });
      return { ...response.data }
    } catch (error) {
      return { success: false }
    }
  },
  createVehicles: async (params) => {
    try {
      const response = await privateServer(params.auth.token).post(`car/create`, { ...params.body });
      return { ...response.data }
    } catch (error) {
      return { success: false }
    }
  },
  deleteVehicles: async (params) => {
    try {
      const response = await privateServer(params.auth.token).delete(`car/delete/${params.body.id}`);
      return { ...response.data }
    } catch (error) {
      console.log(error)
      return { success: false }
    }
  },
  createReport: async (params) => {
    try {

      const response = await publicServer.post(`reports/create`, { ...params });

      return { ...response.data }
    } catch (error) {
      console.log(error)
      return { success: false }
    }
  },
  getReports: async (params) => {
    try {
      const response = await privateServer(params.auth.token).get(`ureport/myreports`);
      return { ...response.data }
    } catch (error) {
      console.log(error)
      return { success: false }
    }
  },
  deleteReport: async (params) => {
    try {
      const response = await privateServer(params.auth).delete(`ureport/delete/${params.id}`);
      return { ...response.data }
    } catch (error) {
      console.log(error)
      return { success: false }
    }
  },
  createChat: async (params) => {
    try {
      const response = await privateServer(params.auth).post(`chat/create`, { ...params.body });
      return { ...response.data }
    } catch (error) {
      console.log(error)
      return { success: false }
    }
  },
  createMessage: async (params) => {
    try {
      const response = await privateServer(params.auth).post(`chat/newmessage`, { ...params.body });
      return { ...response.data }
    } catch (error) {
      console.log(error)
      return { success: false }
    }
  },
  createMessagePublic: async (params) => {
    try {
      const response = await publicServer.post(`chat/newmessage`, { ...params.body });
      return { ...response.data }
    } catch (error) {
      console.log(error)
      return { success: false }
    }
  },
  getChat: async (params) => {
    try {
      console.log(params)
      const response = await privateServer(params.auth).get(`chat/all/${params.body.chat_id}`);
      return { ...response.data }
    } catch (error) {
      console.log(error)
      return { success: false }
    }
  },
  getChatPublic: async (params) => {
    try {
      console.log(params)
      const response = await publicServer.get(`chat/all/${params.body.chat_id}`);
      return { ...response.data }
    } catch (error) {
      console.log(error)
      return { success: false }
    }
  },
  getMyChats: async (params) => {
    try {
      const response = await privateServer(params.auth).get(`chat/mychats`);
      return { ...response.data }
    } catch (error) {
      console.log(error)
      return { success: false }
    }
  },


}
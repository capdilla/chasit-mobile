import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList, AsyncStorage, Dimensions } from 'react-native';
import moment from 'moment';

//redux 
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchUser, logOutUser } from '../reducers/user';
import { getReports, deleteReport } from '../reducers/reports'

import LoginView from '../components/LoginView';
import Icon from 'react-native-vector-icons/Feather';
import ParallaxScrollView from 'react-native-parallax-scroll-view';

import ButtonInfo from '../components/ButtonInfo'
import { Actions } from 'react-native-router-flux';


const HeaderButtons = ({ children, logOut }) => (
  <View style={styles.header}>
    <TouchableOpacity style={{ height: 40, width: 40 }} onPress={() => Navigation.dismissModal()}>
      <Icon name="more-horizontal" color="#1A76AB" size={30} />
    </TouchableOpacity>
    {children}
    <TouchableOpacity
      onPress={() => logOut()}>
      <Icon name="log-out" color="#1A76AB" size={30} />
    </TouchableOpacity>
  </View>
)

const Header = ({ user }) => (
  <View style={styles.big}>
    <View style={[styles.header, { ...alignCenter }]}>
      <View style={[styles.imageContainer]}>
        <Image
          style={{ height: 150, width: 150, borderRadius: 15 }}
          source={{ uri: 'http://wow.zamimg.com/uploads/screenshots/small/449254.jpg' }}
        />
      </View>
    </View>
    <View style={styles.midHeader}>
      <View style={{ left: 95, alignItems: 'flex-start' }}>
        <Text style={{ color: '#2C7FB2', fontSize: 18, textAlign: 'left', fontWeight: '600' }}>{user.name}</Text>
        {/* <Text style={{ color: '#78787d', fontSize: 13, textAlign: 'left', width: 100, fontWeight: '200' }}>Gym Slogan because yes</Text> */}
      </View>
    </View>
  </View >
)

const { width } = Dimensions.get('window');
const RowReport = ({ data, onDelete }) => (

  <TouchableOpacity
    onPress={() => Actions.report_message({ report: data })}
    style={{ flex: 1, height: 130, width: width - 20, ...styleGeneric, ...shadow, top: 10 }}>
    
    <View style={{ flexDirection: 'row', justifyContent: 'space-between', top: 10 }}>
      <View style={{ width: 180, }}>
        <Text style={{ color: '#27518b', fontSize: 18, fontWeight: '200' }}>{data.reason}</Text>
        <Text style={{ fontSize: 13, fontWeight: '200', color: '#086799' }}>{moment(data.createAt).format('MMMM Do YYYY, h:mm:ss a')}</Text>
      </View>
      <TouchableOpacity
        onPress={() => onDelete(data.id)}
        style={styles.trash}>
        <Icon name={"trash"} color={"#27518b"} size={30} />
      </TouchableOpacity>
    </View>
    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 15, top: 15 }}>
      <View>
        <Text style={{ color: '#27518b', fontSize: 19, fontWeight: '200' }}>Marca</Text>
        <Text style={{ fontSize: 13, textAlign: 'center', color: '#086799' }}>{data.vehicle.brand}</Text>
      </View>
      <View>
        <Text style={{ color: '#27518b', fontSize: 19, fontWeight: '200' }}>Modelo</Text>
        <Text style={{ fontSize: 13, textAlign: 'center', color: '#086799' }}>{data.vehicle.model}</Text>
      </View>
      <View>
        <Text style={{ color: '#27518b', fontSize: 19, fontWeight: '200' }}>Placa</Text>
        <Text style={{ fontSize: 13, textAlign: 'center', color: '#086799' }}>{data.vehicle.license}</Text>
      </View>
    </View>

  </TouchableOpacity >
)


const UserView = ({ user, vehicles, logOut, reports, actions }) => (
  <ParallaxScrollView
    backgroundColor="transparent"
    contentBackgroundColor="transparent"
    parallaxHeaderHeight={300}
    renderFixedHeader={() => < HeaderButtons logOut={logOut} />}
    renderForeground={() => (
      <Header user={user} />
    )}>
    <View style={styles.medium}>
      <Text style={{ color: '#2C7FB2', textAlign: 'left' }}>Mis Vehiculos</Text>
      <FlatList
        data={[...user.vehicles, { type: 'add', brand: 'Agregar +', model: 'nuevo vehiculo ' }]}
        keyExtractor={(item, index) => index}
        numColumns={2}
        renderItem={({ item }) =>
          <ButtonInfo
            onPress={() => Actions.modal({ slides: ['Vehicles'], cars: item })}
            textUp={item.brand}
            textDown={item.model}
            style={{ backgroundColor: "#0D74A8" }} />
        }
      />
    </View>
    <View style={styles.medium}>
      <Text style={{ color: '#2C7FB2', textAlign: 'left' }}>Mis Reportes</Text>
      <FlatList
        data={reports.user_report}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) =>
          <RowReport
            onDelete={(id) => actions.deleteReport({ auth: user.token, id })}
            data={item}
          />
        }
      />
    </View>
  </ParallaxScrollView>
);


class User extends Component {

  constructor() {
    super();
    this.logOut = this._logOut.bind(this)
  }

  async _logOut() {
    await AsyncStorage.removeItem('UserData');
    this.props.actions.logOutUser()
  }

  render() {
    const { user, reports } = this.props;
    return (
      <View style={{ flex: 1, backgroundColor: '#C1DBEE' }}>
        {
          user.guest ?
            <LoginView />
            :
            <UserView
              user={user}
              reports={reports}
              actions={this.props.actions}
              logOut={this.logOut}
            />
        }
      </View>
    )
  }
}

const alignCenter = {
  justifyContent: 'center',
  alignItems: 'center'
}

const styleGeneric = {
  backgroundColor: 'transparent',
  borderBottomWidth: 1,
  borderColor: '#94C3E3',
};
const shadow = {
  shadowColor: '#000000',
  shadowOpacity: 0.12,
  shadowRadius: 5,
  elevation: 2,
  shadowOffset: {
    height: 1,
    width: 0
  },
}
const styles = StyleSheet.create({
  big: {
    ...styleGeneric,
    height: 250,
  },
  small: {
    height: 100,
    ...styleGeneric,
    ...alignCenter,
  },
  medium: {
    flex: 1,
    // height: 120,
    paddingVertical: 10,
    ...styleGeneric,
    ...alignCenter,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 25,
    alignItems: 'flex-start',
    paddingTop: 25,
  },
  midHeader: {
    ...alignCenter,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    // top: -15,
    paddingHorizontal: 20
  },
  text: {
    fontSize: 18,
    fontFamily: 'Arial',
    fontWeight: 'bold',
    color: '#91627b',
  },
  trash: {
    flex: 1,
    backgroundColor: 'transparent',
    // height: 50,
    // width: 50,
    ...alignCenter,
    left: 60
  }
})

export default connect(
  state => ({
    user: state.user,
    reports: state.reports
  }),
  dispatch => ({
    actions: bindActionCreators({ fetchUser, logOutUser, getReports, deleteReport }, dispatch)
  })
)(User);
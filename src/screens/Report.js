import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import { Actions } from 'react-native-router-flux'

const Report = () => (
  <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center',backgroundColor:'#C1DBEE' }}>
    <TouchableOpacity
      onPress={() => Actions.report_detail()}
      style={styles.big}>
      <View style={styles.medium}>
        <View style={styles.small}>
          <Icon name={"zap"} color={"#fff"} size={30} />
          <Text style={styles.text}>Report</Text>
        </View>
      </View>
    </TouchableOpacity>
  </View >
);

const alignCenter = {
  justifyContent: 'center',
  alignItems: 'center'
}

const shadow = {
  shadowColor: "#000000",
  shadowOpacity: 0.12,
  shadowRadius: 5,
  elevation: 3,
  shadowOffset: {
    height: 1,
    width: 0
  },
}
const styles = StyleSheet.create({
  big: {
    ...alignCenter,
    ...shadow,
    height: 200,
    width: 200,
    backgroundColor: '#6198ca',//0d467a
    borderRadius: 100
  },
  medium: {
    ...alignCenter,
    ...shadow,
    height: 160,
    width: 160,
    backgroundColor: '#6198ca',
    borderRadius: 100
  },
  small: {
    ...alignCenter,
    ...shadow,
    height: 120,
    width: 120,
    backgroundColor: '#67a2d8',//3c87b6
    borderRadius: 100
  },
  text: {
    fontSize: 18,
    fontFamily: 'Arial',
    fontWeight: 'bold',
    color: '#fff',
    fontSize: 14,
    fontWeight: '200',
    top: 10
  }
})

export default Report;
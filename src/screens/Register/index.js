import React, { Component } from 'react';
import { View, Text, KeyboardAvoidingView, TouchableOpacity, Dimensions } from 'react-native';

import { withState, withHandlers, compose } from 'recompose'
import AppIntroSlider from 'react-native-app-intro-slider';
import { Kohana } from 'react-native-textinput-effects';
import Icon from 'react-native-vector-icons/Feather';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import UserProfile from "./UserProfile"
import Vehicles from './Vehicles';
import { Actions } from 'react-native-router-flux'


//redux 
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { register, fetching, updateVechicles, deleteVechicles, createVechicles } from "../../reducers/user";

const slidess = [
  {
    key: 'UserProfile',
    title: 'Title 1',
    text: 'Description.\nSay something cool',
    backgroundColor: '#165B8D',
  },
  {
    key: 'Vehicles',
    title: 'Title 2',
    text: 'Other cool stuff',
    backgroundColor: '#1A76AB',
  },
  {
    key: 'DoneView',
    title: 'Rocket guy',
    text: 'I\'m already out of descriptions\n\nLorem ipsum bla bla bla',
    backgroundColor: '#329FD8',
  }
];



const DoneView = () => (
  <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
    <Text style={[styles.text, { color: '#fff' }]}> Eso es Todo Presiona Finalizar </Text>
  </View>
)

const Elements = {
  UserProfile,
  Vehicles,
  DoneView
}
const Items = ({ item, onBlur, mainProps }) => {
  const Element = Elements[item.key]
  return (
    <View style={{ flex: 1, backgroundColor: item.backgroundColor, width: item.width }}>
      <Element onBlur={onBlur} mainProps={mainProps} />
    </View>
  )
}

const Button = ({ text }) => (
  <View style={{
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 15,
    shadowColor: "#000000",
    shadowOpacity: 0.12,
    shadowRadius: 5,
    elevation: 2,
    shadowOffset: {
      height: 1,
      width: 0
    },
    backgroundColor: '#67ADDB'
  }}>
    <Text style={[styles.text, { fontSize: 15, color: '#C6F1FF' }]}>{text}</Text>
  </View>
)
class Register extends Component {

  constructor() {
    super()
    this.state = {
      cars: {
        brand: '',
        type: 'car',
        model: '',
        year: '',
        color: '',
        license: ''
      },
      user: {
        name: '',
        mail: '',
        password: ''
      }
    }
    this.onBlur = this._onBlur.bind(this)
  }

  componentWillMount() {
    console.log(this.props.cars)
    const { cars } = this.props;
    if (!cars) {
      return
    }
    if (cars.type == "add") {
      this.setState({
        cars: {
          brand: '',
          type: 'car',
          model: '',
          year: '',
          color: '',
          license: ''
        }
      });
    } else {
      this.setState({ cars: { ...this.props.cars } })
    }

  }

  _onBlur(state, key) {
    this.setState({ [key]: state })
  }

  _register(user) {
    if (this.props.cars && this.props.cars.id) {
      this.props.actions.updateVechicles({
        auth: {
          token: user.token
        },
        body: {
          ...this.state.cars
        }
      })
    } else if (this.props.cars && this.props.cars.type == 'add') {
      this.props.actions.createVechicles({
        auth: {
          token: user.token
        },
        body: {
          ...this.state.cars
        }
      });
    } else {
      this.props.actions.fetching();
      this.props.actions.register({
        ...this.state,
        user: { ...this.state.user, notification_push_user_id: user.notification_push_user_id }
      })
    }
    Actions.pop()
  }

  _getSlides(slides) {

    if (slides) {
      return slidess.filter(slide => slides.includes(slide.key));
    }
    return slidess
  }

  _delete() {
    this.props.actions.deleteVechicles({
      auth: {
        token: this.props.user.token
      },
      body: {
        ...this.state.cars
      }
    })
    Actions.pop()
  }

  render() {
    console.log(this.props.user.notification_push_user_id)
    return (
      <View style={{ flex: 1, backgroundColor: '#C1DBEE' }}>
        <View style={{ width: Dimensions.get("window").width, position: 'absolute', flexDirection: 'row', justifyContent: 'space-between', height: 50, zIndex: 10, backgroundColor: 'transparent' }}>
          <TouchableOpacity
            onPress={() => Actions.pop()}
            style={styles.buttonWithTrash}>
            <Icon name={"x"} color={"#fff"} size={30} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this._delete()}
            style={styles.trash}>
            {this.props.cars && this.props.cars.id && <Icon name={"trash"} color={"#fff"} size={30} />}
          </TouchableOpacity>
        </View>
        <AppIntroSlider
          slides={this._getSlides(this.props.slides)}
          renderItem={item => <Items mainProps={this.state} item={item} onBlur={this.onBlur} />}
          bottomButton
          onSlideChange={this.onSlideChange}
          renderNextButton={() => <Button text={"Siguiente"} />}
          renderDoneButton={() => <Button text={"Finalizar"} />}
          onDone={() => this._register(this.props.user)}
        />
      </View>
    )
  }
}

const alignCenter = {
  justifyContent: 'center',
  alignItems: 'center'
}

const styles = {
  text: {
    fontSize: 18,
    fontFamily: 'Arial',
    fontWeight: 'bold',
    color: '#67ADDB',
    textAlign: 'center'
  },
  buttonWithTrash: {
    flex: 1,
    backgroundColor: 'transparent',
    height: 50,
    width: 50,
    ...alignCenter,
    left: -60
  },
  trash: {
    flex: 1,
    backgroundColor: 'transparent',
    height: 50,
    width: 50,
    ...alignCenter,
    left: 60
  }
}

export default connect(
  state => ({
    register: state.register,
    user: state.user,
    fetching: state.fetching
  }),
  dispatch => ({
    actions: bindActionCreators({ register, fetching, updateVechicles, createVechicles, deleteVechicles }, dispatch)
  })
)(Register);
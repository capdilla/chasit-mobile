import React from 'react';
import { View, Text, KeyboardAvoidingView } from 'react-native';

import { withState, withHandlers, compose } from 'recompose'
import { Kohana } from 'react-native-textinput-effects';
import Icon from 'react-native-vector-icons/Feather';

//redux 
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

const enhance = compose(
  withState('state', 'setState', {
    name: '',
    mail: '',
    password: ''
  }),
  withHandlers({
    onChange: (prop) => (val, key) => {
      let s = { ...prop.state }
      s[key] = val;
      prop.setState(s);

    },
  })
);

const UserProfile = enhance(({ state, onChange, onBlur }) => (
  <View style={{ ...alignCenter, flex: 1 }}>
    <KeyboardAvoidingView
      behavior="padding"
      enabled
      style={{ marginHorizontal: 15, flex: 1, justifyContent: 'center', width: 280, }}
    >
      <Text style={styles.text}> Ingresa tu nombre, mail y clave </Text>
      <View style={{ height: 60, marginVertical: 10, marginHorizontal: 15 }}>
        <Kohana
          style={{ backgroundColor: '#E0EDF6' }}
          value={state.name}
          onBlur={() => onBlur(state, 'user')}
          onChangeText={val => onChange(val, 'name')}
          label={'Nombre'}
          iconClass={Icon}
          iconName={'user'}
          iconColor={'#2C7FB2'}
          labelStyle={{ color: '#5494BF' }}
          inputStyle={{ color: '#5494BF' }}
        />
      </View>
      <View style={{ height: 60, marginVertical: 10, marginHorizontal: 15 }}>
        <Kohana
          style={{ backgroundColor: '#E0EDF6' }}
          value={state.mail}
          onBlur={() => onBlur(state, 'user')}
          onChangeText={val => onChange(val, 'mail')}
          label={'Email'}
          iconClass={Icon}
          iconName={'mail'}
          iconColor={'#2C7FB2'}
          labelStyle={{ color: '#5494BF' }}
          inputStyle={{ color: '#5494BF' }}
        />
      </View>
      <View style={{ height: 60, marginVertical: 10, marginHorizontal: 15 }}>
        <Kohana
          style={{ backgroundColor: '#E0EDF6' }}
          label={'Password'}
          iconClass={Icon}
          iconName={'lock'}
          iconColor={'#2C7FB2'}
          labelStyle={{ color: '#5494BF' }}
          inputStyle={{ color: '#5494BF' }}
          secureTextEntry
          onBlur={() => onBlur(state, 'user')}
          value={state.password}
          onChangeText={val => onChange(val, 'password')}
        />
      </View>
      <Text style={{ color: 'red', textAlign: 'center' }}>{''}</Text>
    </KeyboardAvoidingView>
  </View>
));

const alignCenter = {
  justifyContent: 'center',
  alignItems: 'center'
}

const styles = {
  text: {
    fontSize: 18,
    fontFamily: 'Arial',
    fontWeight: 'bold',
    color: '#67ADDB',
    textAlign: 'center'
  },
}


export default UserProfile;
import React from 'react';
import { View, Text, KeyboardAvoidingView, Switch } from 'react-native';

import { withState, withHandlers, compose } from 'recompose'
import { Kohana } from 'react-native-textinput-effects';
import Icon from 'react-native-vector-icons/Feather';


const inputs = [
  { name: 'brand', label: 'Marca', icon: 'circle' },
  { name: 'model', label: 'Modelo', icon: 'circle' },
  { name: 'year', label: 'Año', icon: 'calendar' },
  { name: 'color', label: 'Color', icon: 'circle' },
  { name: 'license', label: 'Placa', icon: 'hash' },
]

const enhance = compose(

  withState('state', 'setState', ({ mainProps }) => ({ ...mainProps.cars })),
  withHandlers({
    onChange: (prop) => (val, key) => {
      let s = { ...prop.state }
      s[key] = val;
      prop.setState(s);
    },
    onChangeType: (prop) => (val) => {
      let s = { ...prop.state }
      s.type = (val) ? 'moto' : 'car';
      prop.setState(s);
    },
  })
);

const typeS = (state, val) => {
  let s = { ...state }
  s.type = (val) ? 'moto' : 'car';
  return s;
}

const Vehicles = enhance(({ state, onChange, onBlur, onChangeType }) => (
  <View style={{ ...alignCenter, flex: 1 }}>
    <KeyboardAvoidingView
      behavior="padding"
      enabled
      style={{ marginHorizontal: 15, flex: 1, justifyContent: 'center', width: 280, }}
    >
      <Text style={styles.text}> Ingresa un Vehiculo </Text>
      <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
        <Text style={styles.text}> Coche </Text>
        <Switch
          value={(state.type == 'moto')}
          onValueChange={(val) => {
            onChangeType(val)
            return onBlur(typeS(state, val), 'cars')
          }}
          style={{ marginHorizontal: 15, marginVertical: 10, }}
          thumbTintColor={'#67ADDB'}
          onTintColor={'#E0EDF6'} />
        <Text style={styles.text}> Moto </Text>
      </View>

      {
        inputs.map((input, key) =>
          <View key={key} style={{ height: 60, marginVertical: 10, marginHorizontal: 15 }}>
            <Kohana
              style={{ backgroundColor: '#E0EDF6' }}
              value={state[input.name].toString()}
              onBlur={() => onBlur(state, 'cars')}
              onChangeText={val => onChange(val, input.name)}
              label={input.label}
              iconClass={Icon}
              iconName={input.icon}
              iconColor={'#2C7FB2'}
              labelStyle={{ color: '#5494BF' }}
              inputStyle={{ color: '#5494BF' }}
            />
          </View>
        )
      }
      <Text style={{ color: 'red', textAlign: 'center' }}>{''}</Text>
    </KeyboardAvoidingView>
  </View>
));

const alignCenter = {
  justifyContent: 'center',
  alignItems: 'center'
}

const styles = {
  text: {
    fontSize: 18,
    fontFamily: 'Arial',
    fontWeight: 'bold',
    color: '#67ADDB',
    textAlign: 'center'
  },
}
export default Vehicles;
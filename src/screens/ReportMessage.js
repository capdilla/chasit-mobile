import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { withState, withHandlers, compose, branch, renderComponent } from 'recompose'
import moment from 'moment';
import Icon from 'react-native-vector-icons/Feather';
import { Actions } from 'react-native-router-flux'
import ParallaxScrollView from 'react-native-parallax-scroll-view';

const enhace = compose(

);

// const report = {
//   "createAt": "2018-06-07T05:27:02.842Z",
//   "id": "faa577e9-ebdb-4d65-888d-6fac31cd0a57",
//   "reason": "Vehiculo mal estacionado",
//   user_notification_id: 's',
//   "vehicle": {
//     "brand": "toyota",
//     "color": "gris",
//     "id": "101783e6-cadb-4ef5-ab37-5638a44988ba",
//     "license": "ES1993",
//     "model": "Corolla",
//     "type": "car",
//     "user_id": "5e11c9d9-b430-4484-91fb-55a3a7f54136",
//     "year": 1997
//   }
// }

const ReportMessage = enhace(({ report }) => (
  <View style={{ flex: 1, backgroundColor: '#C1DBEE', }}>
    <ParallaxScrollView
      backgroundColor="#C1DBEE"
      contentBackgroundColor="#C1DBEE"
      parallaxHeaderHeight={0}
    >

      <TouchableOpacity
        onPress={() => Actions.pop()}
        style={{ backgroundColor: 'transparent', height: 50, width: 50, zIndex: 10, ...alignCenter }}>
        <Icon name={"x"} color={"#6198ca"} size={30} />
      </TouchableOpacity>
      <View style={{ marginHorizontal: 15, marginTop: 15 }}>

        <View style={styles.div}>
          <Text style={styles.title}>{report.reason}</Text>
          <Text style={styles.description}>
            {
              moment(report.createAt)
                .format('MMMM Do YYYY, h:mm:ss a')
            }
          </Text>
        </View>
        <View style={styles.div}>
          <Text style={styles.title}>Marca</Text>
          <Text style={styles.description}>{report.vehicle.brand}</Text>
        </View>
        <View style={styles.div}>
          <Text style={styles.title}>Modelo</Text>
          <Text style={styles.description}>{report.vehicle.model}</Text>
        </View>
        <View style={styles.div}>
          <Text style={styles.title}>Color</Text>
          <Text style={styles.description}>{report.vehicle.color}</Text>
        </View>
        <View style={styles.div}>
          <Text style={styles.title}>Año</Text>
          <Text style={styles.description}>{report.vehicle.year}</Text>
        </View>
        <View style={styles.div}>
          <Text style={styles.title}>Matricula</Text>
          <Text style={styles.description}>{report.vehicle.license}</Text>
        </View>
        <View style={[styles.div, { marginVertical: 0 }]}>
          <Text style={styles.title}>{((report.user_notification_id && report.user_notification_id != '') ? ' Puedes ' : 'No Puedes ') + 'Contactar'}</Text>
          {
            (report.user_notification_id && report.user_notification_id != '') ?
              <TouchableOpacity
                onPress={() => Actions.chat({ isNew: true, user_notification_id: report.user_notification_id })}
                style={[styles.submit, { marginVertical: 10, top: 5 }]}>
                <Text style={[styles.text, { fontSize: 15, color: '#C6F1FF' }]}>Contactar</Text>
              </TouchableOpacity>
              :
              null
          }
        </View>
      </View>
    </ParallaxScrollView>
  </View>
));

const alignCenter = {
  justifyContent: 'center',
  alignItems: 'center'
}

const styleGeneric = {
  backgroundColor: 'transparent',
  borderBottomWidth: 1,
  borderColor: '#94C3E3',
};
const shadow = {
  shadowColor: '#000000',
  shadowOpacity: 0.12,
  shadowRadius: 5,
  elevation: 2,
  shadowOffset: {
    height: 1,
    width: 0
  },
}

const styles = StyleSheet.create({
  title: {
    color: '#27518b',
    fontSize: 19,
    fontWeight: '200'
  },
  div: {
    ...styleGeneric,
    marginVertical: 15
  },
  description: {
    fontSize: 13,
    color: '#086799',
    marginVertical: 10
  },
  submit: {
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 15,
    shadowColor: "#000000",
    shadowOpacity: 0.12,
    shadowRadius: 5,
    elevation: 2,
    top: -10,
    shadowOffset: {
      height: 1,
      width: 0
    },
    backgroundColor: '#27518b'
  },
})

export default ReportMessage

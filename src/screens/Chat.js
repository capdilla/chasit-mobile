import React, { Component } from 'react';
import { View, Text, Platform, TouchableOpacity } from 'react-native';

import { GiftedChat } from 'react-native-gifted-chat'

import update from 'react-addons-update';

import io from 'socket.io-client';

import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Feather';

import apis, { url } from '../actions/apis';

//redux 
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setNotificationPushId } from '../reducers/user';

const chat_id = Platform.select({
  ios: '9722411e-1662-465a-97e0-b4925203216c',
  android: 'cb2e3793-1f19-4edf-813d-725a0e2ad46c'
});



class Chat extends Component {

  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      loadEarlier: true,
      typingText: null,
      isLoadingEarlier: false,
      isNew: false,
      chat_main_id: ''
    };

    this.onSend = this._onSend.bind(this)
    this.socket = null;

    this._isAlright = null;
  }


  componentDidMount() {
    this.setState({ isNew: this.props.isNew, chat_main_id: this.props.chat_main_id })
    console.log(this.props)
    if (this.props.chat_main_id != '') {
      this._getData({ chat_id: this.props.chat_main_id })
    }
  }

  _getData({ chat_id }) {
    apis.getChat({
      auth: this.props.user.token,
      body: {
        chat_id
      }
    })
      .then(data => {
        this.setState({ messages: data.chats })
        this._socket(data)
        console.log(data)
      })
  }

  //wait for the new messages and subscribe
  _socket(params) {

    this.socket = io.connect(`${url}chat`);

    this.socket.on('connect', () => {
      // Connected, let's sign-up for to receive messages for this room
      this.socket.emit('room',
        {
          room: params.chat_main_id,
        }
      );
    });
    this.socket.on('message', (data) => {
      console.log(data)
      this.setState(previousState => ({
        messages: GiftedChat.append(previousState.messages, data.new_data),
      }))
    })
  }

  componentWillUnmount() {
    if (this.socket) {
      this.socket.disconnect();
      this.socket.emit('disconnect', 'plis');
    }

  }


  _onSend(message) {
    const { user, user_notification_id } = this.props
    const { isNew, chat_main_id } = this.state
    if (isNew) {
      apis.createChat({
        auth: user.token,
        body: {
          text: message[0].text,
          notification_push_user_id: user_notification_id
        }
      })
        .then(response => {
          this._socket({ chat_main_id: response.chat_main_id })
          this.setState(previousState => ({
            messages: GiftedChat.append(previousState.messages, message),
            isNew: false,
            chat_main_id: response.chat_main_id
          }))
        })
      return
    } else {
      apis.createMessage({
        auth: user.token,
        body: {
          text: message[0].text,
          chat_main_id: chat_main_id
        }
      })
    }
  }

  render() {
    const { user } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <TouchableOpacity
          onPress={() => Actions.pop()}
          style={{ backgroundColor: 'transparent', height: 50, width: 50, zIndex: 10, ...alignCenter }}>
          <Icon name={"x"} color={"#6198ca"} size={30} />
        </TouchableOpacity>
        <GiftedChat
          messages={this.state.messages}
          onSend={messages => this.onSend(messages)}
          user={{
            _id: user.id,
          }}
        />
      </View>

    );
  }

}
const alignCenter = {
  justifyContent: 'center',
  alignItems: 'center'
}

Chat.defaultProps = {
  isNew: false,
  chat_main_id: ''
}

export default connect(
  state => ({
    user: state.user,
  }),
  dispatch => ({
    actions: bindActionCreators({ setNotificationPushId }, dispatch)
  })
)(Chat)
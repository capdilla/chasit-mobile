import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, FlatList } from 'react-native';
import { withState, withHandlers, compose, branch, renderComponent, lifecycle } from 'recompose'
import moment from 'moment';
import Icon from 'react-native-vector-icons/Feather';
import { Actions } from 'react-native-router-flux'
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import index from './Register';


import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getChats } from '../reducers/chat'

const enhace = compose(
  lifecycle({
    componentDidMount() {
      const { user } = this.props
      this.props.actions.getChats({ auth: user.token })
    }
  }),
  withState('refresh', 'setRefresh', false),
  withHandlers({
    onRefresh: (props) => async () => {
      props.setRefresh(true)
      await props.actions.getChats({ auth: props.user.token })
      props.setRefresh(false)
    }

  })
);

// const user = { mail: 'carlos@mail.com' }

const Item = ({ data, onEnter, user }) => (
  <TouchableOpacity
    onPress={() => onEnter(data.id)}
    style={styles.div}>
    <Text style={styles.title}>{(data.user1.mail == user.mail) ? data.user2.name : data.user1.name}</Text>
    <Text style={styles.description}>{moment(data.createdAt).format('MMMM Do YYYY, h:mm:ss a')}</Text>
  </TouchableOpacity>
)

const ChatList = enhace(({ chats, refresh, onRefresh, user }) => (
  <View style={{ flex: 1, backgroundColor: '#C1DBEE', }}>
    <FlatList
      onRefresh={() => onRefresh()}
      refreshing={refresh}
      data={chats.chats}
      keyExtractor={(item, index) => index.toString()}
      renderItem={({ item }) =>
        <Item
          user={user}
          onEnter={(id) => Actions.chat({ chat_main_id: id })}
          data={item} />
      }
    />
  </View>
));

const alignCenter = {
  justifyContent: 'center',
  alignItems: 'center'
}

const styleGeneric = {
  backgroundColor: 'transparent',
  borderBottomWidth: 1,
  borderColor: '#94C3E3',
};
const shadow = {
  shadowColor: '#000000',
  shadowOpacity: 0.12,
  shadowRadius: 5,
  elevation: 2,
  shadowOffset: {
    height: 1,
    width: 0
  },
}

const styles = StyleSheet.create({
  title: {
    color: '#27518b',
    fontSize: 19,
    fontWeight: '200'
  },
  div: {
    ...styleGeneric,
    marginVertical: 15,
    paddingHorizontal: 15,
    height: 50
  },
  description: {
    fontSize: 13,
    color: '#086799',
    marginVertical: 10
  },
  submit: {
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 15,
    shadowColor: "#000000",
    shadowOpacity: 0.12,
    shadowRadius: 5,
    elevation: 2,
    top: -10,
    shadowOffset: {
      height: 1,
      width: 0
    },
    backgroundColor: '#27518b'
  },
})



export default connect(
  (state) => ({
    chats: state.chat,
    user: state.user
  }),
  (dispatch) => ({
    actions: bindActionCreators({ getChats }, dispatch)
  })
)(ChatList)

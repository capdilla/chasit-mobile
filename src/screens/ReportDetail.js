import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, FlatList, Switch } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import { Kohana } from 'react-native-textinput-effects';
import { Actions } from 'react-native-router-flux'
import { withState, withHandlers, compose, branch, renderComponent } from 'recompose'
// import { createAnimatableComponent } from 'react-native-animatable';

// const FlatListAnimated = createAnimatableComponent(FlatList);

//redux 
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createReport, defaultReport } from '../reducers/reports';

const FlatListAnimated = FlatList;

const types = [
  { id: 1, name: 'Lucen encendidas' },
  { id: 2, name: 'Vehiculo mal estacionado' },
  { id: 3, name: 'Estan dañando el vehiculo' },
  { id: 4, name: 'Le estan robando' },
  { id: 5, name: 'Otro' },
]
//f5e9ed 
const Row = ({ data, reason, setReason }) => {
  let backgroundColor = (data.name == reason) ? '#086799' : '#67ADDB'
  return (
    <TouchableOpacity
      onPress={() => setReason(data.name)}
      style={[styles.row, { backgroundColor }]}>
      <Text style={[styles.text, { fontSize: 15 }]}>{data.name}</Text>
    </TouchableOpacity >
  )
};

const ResultView = ({ report, actions }) => (
  <View style={{ flex: 1, backgroundColor: '#C1DBEE' }}>

    <TouchableOpacity
      onPress={() => actions.defaultReport()}
      style={{ backgroundColor: 'transparent', height: 50, width: 50, zIndex: 10, ...alignCenter }}>
      <Icon name={"x"} color={"#6198ca"} size={30} />
    </TouchableOpacity>

    <View style={{ flex: 1, ...alignCenter }}>
      <Text style={[styles.text, { color: report.create.success ? '#4991C3' : 'red', textAlign: 'center' }]} >{report.create.message}</Text>
    </View>

    <TouchableOpacity
      onPress={() => actions.defaultReport()}
      style={styles.submit}
    >
      <Text style={[styles.text, { fontSize: 15, color: '#C6F1FF' }]}>OK</Text>
    </TouchableOpacity>

  </View >
)

const enhance = compose(
  branch(
    props => props.report.create.done,
    renderComponent(ResultView)
  ),
  withState('license', 'setLicense', ''),
  withState('reason', 'setReason', 'Lucen encendidas'),
  withState('contact', 'setContact', true),
)

const ReportDetail = enhance(({ license, reason, setLicense, setReason, actions, contact, setContact, user }) => (
  <View style={{ flex: 1, backgroundColor: '#C1DBEE' }}>
    <TouchableOpacity
      onPress={() => Actions.pop()}
      style={{ backgroundColor: 'transparent', height: 50, width: 50, zIndex: 10, ...alignCenter }}>
      <Icon name={"x"} color={"#6198ca"} size={30} />
    </TouchableOpacity>
    <View style={{ height: 60, marginVertical: 10, marginHorizontal: 15 }}>
      <Kohana
        style={{ backgroundColor: '#E0EDF6' }}
        label={'Placa'}
        iconClass={Icon}
        iconName={'hash'}
        iconColor={'#2C7FB2'}
        value={license}
        onChangeText={(val) => setLicense(val)}
        labelStyle={{ color: '#5494BF' }}
        inputStyle={{ color: '#5494BF' }}
      />
    </View>

    <View style={{ flex: 1, marginVertical: 10, marginHorizontal: 15 }}>
      <FlatListAnimated
        data={types}
        animation="bounceIn"
        renderItem={({ item, index }) =>
          <Row
            key={index}
            setReason={setReason}
            reason={reason}
            data={item} />
        }
        contentInsetAdjustmentBehavior="automatic"
        keyExtractor={item => item.id.toString()}
        duration={2000}
        ListHeaderComponent={({ section }) => (
          <View style={{ ...alignCenter }}>
            <Text style={[styles.text, { color: '#4991C3' }]} >Seleccione un motivo </Text>
          </View>
        )}
      />
    </View>
    <View style={{ ...alignCenter }}>
      <Text style={[styles.text, { color: '#4991C3' }]} >Quieres que puedan contactarte? </Text>
    </View>
    <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
      <Text style={[styles.text, { color: '#4991C3' }]}> No </Text>
      <Switch
        value={contact}
        onValueChange={(val) => setContact(val)}
        style={{ marginHorizontal: 15, marginVertical: 10, }}
        thumbTintColor={'#67ADDB'}
        onTintColor={'#E0EDF6'} />
      <Text style={[styles.text, { color: '#4991C3' }]}> Si </Text>
    </View>

    <TouchableOpacity
      onPress={() => actions.createReport({ reason, license, contact, user_notification_id: user.notification_push_user_id })}
      style={styles.submit}>
      <Text style={[styles.text, { fontSize: 15, color: '#C6F1FF' }]}>Reportar</Text>
    </TouchableOpacity>

  </View >
))


const alignCenter = {
  justifyContent: 'center',
  alignItems: 'center'
}

const shadow = {
  shadowColor: "#000000",
  shadowOpacity: 0.12,
  shadowRadius: 5,
  elevation: 2,
  shadowOffset: {
    height: 1,
    width: 0
  },
}
const styles = StyleSheet.create({
  big: {
    ...alignCenter,
    ...shadow,
    height: 200,
    width: 200,
    backgroundColor: '#f9f7f8',
    borderRadius: 100
  },
  medium: {
    ...alignCenter,
    ...shadow,
    height: 160,
    width: 160,
    backgroundColor: '#f8f8fa',
    borderRadius: 100
  },
  small: {
    ...alignCenter,
    ...shadow,
    height: 120,
    width: 120,
    backgroundColor: '#ffffff',
    borderRadius: 100
  },
  text: {
    fontSize: 18,
    fontFamily: 'Arial',
    fontWeight: 'bold',
    color: '#C6F1FF',
  },
  submit: {
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 15,
    shadowColor: "#000000",
    shadowOpacity: 0.12,
    shadowRadius: 5,
    elevation: 2,
    top: -10,
    shadowOffset: {
      height: 1,
      width: 0
    },
    backgroundColor: '#27518b'
  },
  row: {
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 5,
    ...shadow,
    backgroundColor: '#67ADDB'
  }

})

export default connect(
  state => ({
    report: state.reports,
    user: state.user
  }),
  dispatch => ({
    actions: bindActionCreators({ createReport, defaultReport }, dispatch)
  })
)(ReportDetail);